# Japanese translation of Yumex
# Copyright (C) 2005, 2006 Tim Lauridsen
# This file is distributed under the same license as the Yumex package.
# MATSUURA Takanori <t.matsuu@gmail.com>, 2005-2007.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: yumex 2.0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-10-26 11:23+0200\n"
"PO-Revision-Date: 2007-10-16 19:01+0900\n"
"Last-Translator: MATSUURA Takanori <t.matsuu@gmail.com>\n"
"Language-Team: Japanese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/callbacks.py:69
#, python-format
msgid "Getting : %s"
msgstr "取得中: %s"

#: ../src/callbacks.py:164 ../src/yumapi.py:204
msgid "Removing"
msgstr "削除中"

#: ../src/callbacks.py:166
msgid "Cleanup"
msgstr "片付け"

#: ../src/callbacks.py:184
#, python-format
msgid "Erased: %s"
msgstr "消去: %s"

#: ../src/callbacks.py:205
#, python-format
msgid "Processing metadata from : %s"
msgstr "メタデータを処理中: %s"

#: ../src/callbacks.py:207
msgid "Processing metadata"
msgstr "メタデータを処理中"

#: ../src/callbacks.py:234
#, fuzzy
msgid "installed"
msgstr "インストール"

#: ../src/callbacks.py:235
#, fuzzy
msgid "updated"
msgstr "更新"

#: ../src/callbacks.py:236
#, fuzzy
msgid "obsoleted"
msgstr "廃止"

#: ../src/callbacks.py:237
#, fuzzy
msgid "erased"
msgstr "消去"

#: ../src/callbacks.py:240
#, python-format
msgid "---> Package %s.%s %s:%s-%s set to be %s"
msgstr "---> パッケージ%s.%s %s:%s-%sを%sに設定"

#: ../src/callbacks.py:247
msgid "--> Running transaction check"
msgstr "--> トランザクションチェックを実行中"

#: ../src/callbacks.py:251
msgid "--> Restarting Dependency Resolution with new changes."
msgstr "--> 新しい変更を適用して、依存関係の解決を再開。"

#: ../src/callbacks.py:255
msgid "--> Finished Dependency Resolution"
msgstr "--> 依存関係の解決終了"

#: ../src/callbacks.py:259
#, python-format
msgid "--> Processing Dependency: %s for package: %s"
msgstr "--> 依存関係を確認中: %s 対象パッケージ: %s"

#: ../src/callbacks.py:264
#, python-format
msgid "--> Unresolved Dependency: %s"
msgstr "--> 解決不能な依存関係: %s"

#: ../src/callbacks.py:268
#, python-format
msgid "--> Processing Conflict: %s conflicts %s"
msgstr "--> 衝突を処理中: %sは%sと衝突"

#: ../src/callbacks.py:271
msgid "--> Populating transaction set with selected packages. Please wait."
msgstr ""
"--> 選択されたパッケージで用いるトランザクション・セットを実装中。しばらくお"
"待ちください。"

#: ../src/callbacks.py:274
#, python-format
msgid "---> Downloading header for %s to pack into transaction set."
msgstr ""
"---> トランザクション・セットに使用するため、%sのヘッダをダウンロード中。"

#: ../src/dialogs.py:196
msgid "Packages to Process"
msgstr "処理するパッケージ"

#: ../src/dialogs.py:212 ../src/views.py:474
msgid "Name"
msgstr "名前"

#: ../src/dialogs.py:213 ../src/yumex.glade.h:24
msgid "Arch"
msgstr "Arch"

#: ../src/dialogs.py:214 ../src/views.py:61
msgid "Ver"
msgstr "バージョン"

#. Setup reponame & repofile column's
#: ../src/dialogs.py:215 ../src/views.py:438 ../src/views.py:473
msgid "Repository"
msgstr "リポジトリ"

#: ../src/dialogs.py:216 ../src/views.py:61
msgid "Size"
msgstr "サイズ"

#: ../src/dialogs.py:348
#, python-format
msgid "About %s"
msgstr "%sについて"

#: ../src/gui.py:66
#, python-format
msgid ""
"Updating : %s\n"
"\n"
msgstr ""
"更新: %s\n"
"\n"

#: ../src/gui.py:73
#, python-format
msgid ""
"Obsoleting : %s\n"
"\n"
msgstr ""
"除去: %s\n"
"\n"

#: ../src/gui.py:88
msgid ""
"Requires:\n"
"\n"
msgstr ""
"要求するもの:\n"
"\n"

#: ../src/gui.py:103 ../src/gui.py:113
#, python-format
msgid "Can not read the %s attribute"
msgstr "%s属性を読むことができません。"

#: ../src/gui.py:369
msgid "Show All Packages"
msgstr "全てのパッケージを表示"

#: ../src/gui.py:370
msgid "Show Package Updates"
msgstr "更新パッケージを表示"

#: ../src/gui.py:371
msgid "Show available Packages"
msgstr "利用可能なパッケージを表示"

#: ../src/gui.py:372
msgid "Show Installed Packages"
msgstr "インストールされたパッケージを表示"

#. Setup Vertical Toolbar
#: ../src/gui.py:381
msgid "Package View"
msgstr "パッケージ"

#: ../src/gui.py:382
msgid "Group View"
msgstr "グループ"

#: ../src/gui.py:383
msgid "Package Queue View"
msgstr "パッケージキュー"

#: ../src/gui.py:385
msgid "Repository Selection View"
msgstr "リポジトリ選択"

#: ../src/gui.py:386
msgid "Output View"
msgstr "出力"

#.
#. [1]           [2]               [3]             [4]     [5]     [6]
#. -------------------------------------------------------------------------
#: ../src/misc.py:93
msgid "RPM Groups"
msgstr "RPMグループ"

#: ../src/misc.py:94
msgid "Repositories"
msgstr "リポジトリ"

#: ../src/misc.py:95 ../src/views.py:61
msgid "Architecture"
msgstr "アーキテクチャ"

#: ../src/misc.py:96
msgid "Sizes"
msgstr "サイズ"

#: ../src/misc.py:97
msgid "Age"
msgstr " 日数"

#: ../src/misc.py:107
msgid "Mandatory"
msgstr "必須"

#: ../src/misc.py:108
msgid "Default"
msgstr "標準"

#: ../src/misc.py:109
msgid "Optional"
msgstr "オプション"

#: ../src/misc.py:110
msgid "Conditional"
msgstr "条件つき"

#: ../src/misc.py:118
msgid "Programming:"
msgstr "プログラミング:"

#: ../src/misc.py:121
msgid "Translation:"
msgstr "翻訳:"

#: ../src/misc.py:142
msgid "Special Thanks To:"
msgstr "謝辞:"

#: ../src/misc.py:359
msgid "Package Queue:"
msgstr "パッケージキュー:"

#: ../src/misc.py:364
#, python-format
msgid " Packages to %s"
msgstr "%sするパッケージ"

#: ../src/misc.py:371
#, python-format
msgid " Groups to %s"
msgstr "%sするグループ"

#: ../src/misc.py:435
#, python-format
msgid "Wrong file application (%s)"
msgstr "誤ったファイルアプリケーション (%s)"

#: ../src/misc.py:437
#, python-format
msgid "Wrong file type (%s)"
msgstr "誤ったファイルタイプ (%s)"

#: ../src/misc.py:439
#, python-format
msgid "Wrong file version (%s)"
msgstr "誤ったファイルバージョン (%s)"

#: ../src/misc.py:610
msgid "Current Settings :"
msgstr "現在の設定: "

#: ../src/views.py:40
msgid "Categories"
msgstr "カテゴリ"

#: ../src/views.py:61 ../src/views.py:91
msgid "Package"
msgstr "パッケージ"

#: ../src/views.py:61 ../src/views.py:94 ../src/views.py:218
msgid "Summary"
msgstr "概要"

#: ../src/views.py:61
msgid "Repo"
msgstr "Repo"

#: ../src/views.py:92
msgid "Arch."
msgstr "Arch."

#: ../src/views.py:93
msgid "Ver."
msgstr "バージョン"

#: ../src/views.py:95
msgid "Repo."
msgstr "Repo."

#: ../src/views.py:96
msgid "Size."
msgstr "サイズ"

#: ../src/views.py:213
msgid "Packages"
msgstr "パッケージ"

#: ../src/views.py:255
msgid "<b>Packages To Update</b>"
msgstr "<b>更新するパッケージ</b>"

#: ../src/views.py:259
msgid "<b>Packages To Install</b>"
msgstr "<b>インストールするパッケージ</b>"

#: ../src/views.py:263
msgid "<b>Packages To Remove</b>"
msgstr "<b>削除するパッケージ</b>"

#: ../src/views.py:438
msgid "Filename"
msgstr "ファイル名"

#: ../src/views.py:571
msgid "Plugin"
msgstr "プラグイン"

#: ../src/yumapi.py:67 ../src/yumapi.py:114
msgid "Setup Yum : Transaction Set"
msgstr "Yum準備: トランザクションセット"

#: ../src/yumapi.py:69 ../src/yumapi.py:116
msgid "Setup Yum : RPM Db."
msgstr "Yum準備: RPMデータベース"

#. -> Repo Setup
#: ../src/yumapi.py:72 ../src/yumapi.py:119
msgid "Setup Yum : Repositories."
msgstr "Yum準備: リポジトリ"

#. -> Sack Setup
#: ../src/yumapi.py:75 ../src/yumapi.py:122
msgid "Setup Yum : Package Sacks"
msgstr "Yum準備: パッケージ一式"

#. -> Updates Setup
#. -> Sack Setup
#: ../src/yumapi.py:78 ../src/yumapi.py:125
msgid "Setup Yum : Updates"
msgstr "Yum準備: 更新"

#. -> Group Setup
#. -> Sack Setup
#: ../src/yumapi.py:82 ../src/yumapi.py:128
msgid "Setup Yum : Groups"
msgstr "Yum準備: グループ"

#: ../src/yumapi.py:84 ../src/yumapi.py:130
msgid "Setup Yum : Base setup completed"
msgstr "Yum準備: 基本設定完了"

#: ../src/yumapi.py:138
#, python-format
msgid "Loaded update Metadata from %s "
msgstr "%sから更新されたメタデータを読み込みました"

#: ../src/yumapi.py:140
#, python-format
msgid "No update Metadata found for %s "
msgstr "%sの更新メタデータが見つかりませんでした"

#: ../src/yumapi.py:159 ../src/yumex.py:601
msgid "Preparing for install/remove/update"
msgstr "インストール/削除/更新の準備中"

#: ../src/yumapi.py:163
msgid "--> Preparing for install"
msgstr "--> インストールの準備中.."

#: ../src/yumapi.py:168
msgid "--> Preparing for remove"
msgstr "--> 削除の準備中"

#: ../src/yumapi.py:175
msgid "--> Preparing for a full update"
msgstr "--> 全更新の準備中"

#: ../src/yumapi.py:178
msgid "--> Preparing for a partial update"
msgstr "--> 部分更新の準備中"

#: ../src/yumapi.py:180
msgid "--> Adding all obsoletion to transaction"
msgstr "--> 全ての廃止パッケージをトランザクションに追加中"

#: ../src/yumapi.py:202 ../src/yumgui/callbacks.py:62
#: ../src/yumgui/callbacks.py:63 ../src/yumgui/callbacks.py:65
msgid "Installing"
msgstr "インストール"

#: ../src/yumapi.py:203 ../src/yumgui/callbacks.py:60
msgid "Updating"
msgstr "更新"

#: ../src/yumapi.py:205
msgid "Installing for dependencies"
msgstr "依存関係を解決するためインストール"

#: ../src/yumapi.py:206
msgid "Updating for dependencies"
msgstr "依存関係を解決するため更新"

#: ../src/yumapi.py:207
msgid "Removing for dependencies"
msgstr "依存関係を解決するため削除"

#: ../src/yumapi.py:253
#, python-format
msgid "Failure getting %s: "
msgstr "%sの取得に失敗: "

#: ../src/yumapi.py:275
msgid "Downloading Packages:"
msgstr "パッケージをダウンロード中"

#: ../src/yumapi.py:291 ../src/yumapi.py:299
msgid "Error Downloading Packages:\n"
msgstr "パッケージをダウンロード中にエラー:\n"

#. Check GPG signatures
#: ../src/yumapi.py:306
msgid "Checking GPG Signatures:"
msgstr "GPG署名を確認中"

#: ../src/yumapi.py:311
msgid "Error checking package signatures:\n"
msgstr "パッケージ署名を確認中にエラー:\n"

#: ../src/yumapi.py:317
msgid "Traceback in checking package signatures:\n"
msgstr "パッケージ署名の確認を追跡:\n"

#: ../src/yumapi.py:324
msgid "Running Transaction Test"
msgstr "トランザクションテストを実行中"

#: ../src/yumapi.py:342
msgid "Finished Transaction Test"
msgstr "トランザクションテスト完了"

#: ../src/yumapi.py:344
msgid "Transaction Check Error: "
msgstr "トランザクション確認エラー: "

#: ../src/yumapi.py:349
msgid "Transaction Test Succeeded"
msgstr "トランザクションテスト成功"

#: ../src/yumapi.py:365
msgid "Running Transaction"
msgstr "トランザクションを実行中"

#. print "po: %s userid: %s hexkey: %s " % (str(po),userid,hexkeyid)
#: ../src/yumapi.py:449
#, python-format
msgid "Do you want to import GPG Key : %s \n"
msgstr "鍵を併合しますか: %s\n"

#: ../src/yumapi.py:451
#, python-format
msgid "Needed by %s"
msgstr "%sに必要とされています"

#: ../src/yumex.py:63
msgid "Cant Quit while running RPM Transactions"
msgstr "RPMトランザクションを実行中は終了できません"

#: ../src/yumex.py:68
msgid "Quiting, please wait !!!!!!"
msgstr "終了中, お待ち下さい!!!!!!"

#: ../src/yumex.py:158
msgid "Yum has not been initialized yet"
msgstr "Yumがまだ初期化されていません"

#. Check there are any packages in the queue
#: ../src/yumex.py:164
msgid "No packages in queue"
msgstr "キューにパッケージはありません"

#: ../src/yumex.py:241
#, python-format
msgid "You are about to add %s packages\n"
msgstr "%s個のパッケージをを追加しようとしています\n"

#: ../src/yumex.py:242 ../src/yumex.py:254
msgid "It will take some time\n"
msgstr "ある程度時間がかかります\n"

#: ../src/yumex.py:243 ../src/yumex.py:255
msgid "do you want to continue ?"
msgstr "続けますか?"

#: ../src/yumex.py:253
#, python-format
msgid "You are about to remove %s packages\n"
msgstr "%s個のパッケージをを削除しようとしています\n"

#: ../src/yumex.py:359
#, python-format
msgid "Selected the %s profile"
msgstr "プロファイル%sが選択されました"

#: ../src/yumex.py:379
#, python-format
msgid "Profile : %s saved ok"
msgstr "プロファイル: %sを保存"

#: ../src/yumex.py:381
#, python-format
msgid "Profile : %s save failed"
msgstr "プロファイル: %sの保存に失敗"

#: ../src/yumex.py:386
msgid "Create New Profile"
msgstr "新規プロファイルを作成"

#: ../src/yumex.py:386
msgid "Name of new profile"
msgstr "新規プロファイル名"

#: ../src/yumex.py:390
#, python-format
msgid "Profile : %s created ok"
msgstr "プロファイル: %sを作成"

#: ../src/yumex.py:393
#, python-format
msgid "Profile : %s creation failed"
msgstr "プロファイル: %sの作成に失敗"

#: ../src/yumex.py:397
msgid "Cleaning up all yum metadata"
msgstr "全てのYumメタデータを消去中"

#: ../src/yumex.py:424
msgid "Yum Config Setup"
msgstr "Yum設定"

#: ../src/yumex.py:440
msgid "GUI Setup Completed"
msgstr "GUI設定完了"

#: ../src/yumex.py:482 ../src/yumex.py:616 ../src/yumex.py:671
#: ../src/yumex.py:779 ../src/yumex.py:787 ../src/yumex.py:806
#: ../src/yumex.glade.h:30
msgid "Error"
msgstr "エラー"

#: ../src/yumex.py:482
msgid "Error in Yum Setup"
msgstr "Yum設定のエラー"

#. prepare package lists
#. -> List setup
#: ../src/yumex.py:487
msgid "Building Package Lists"
msgstr "パッケージリストを構築中"

#: ../src/yumex.py:489
msgid "Building Package Lists Completed"
msgstr "パッケージリストを構築完了"

#: ../src/yumex.py:490
msgid "Building Groups Lists"
msgstr "グループリストを構築中"

#: ../src/yumex.py:493
msgid "Building Group Lists Completed"
msgstr "グループリストを構築完了"

#: ../src/yumex.py:525
#, python-format
msgid "Getting packages : %s"
msgstr "パッケージを取得中: %s"

#: ../src/yumex.py:529
#, python-format
msgid "Found %d %s packages"
msgstr "%d個の%sパッケージを発見"

#. -> Sort Lists
#: ../src/yumex.py:532
msgid "Sorting packages"
msgstr "パッケージを選別中"

#: ../src/yumex.py:534
msgid "Population view with packages"
msgstr "表示にパッケージを投入"

#: ../src/yumex.py:540
msgid "Population Completed"
msgstr "投入完了"

#: ../src/yumex.py:547
msgid "Package View Population"
msgstr "パッケージ表示の投入"

#: ../src/yumex.py:559
msgid "Package View Population Completed"
msgstr "パッケージ表示を投入完了"

#: ../src/yumex.py:565
msgid "Category View Population"
msgstr "カテゴリ表示の投入"

#: ../src/yumex.py:584
msgid "Category View Population Completed"
msgstr "カテゴリ表示を投入完了"

#: ../src/yumex.py:609
msgid "Processing Packages in queue"
msgstr "キューにあるパッケージを処理中"

#: ../src/yumex.py:615
msgid "Error in Dependency Resolution"
msgstr "依存関係の解決中にエラーが発生しました"

#: ../src/yumex.py:632
msgid "No packages selected"
msgstr "パッケージが選択されていません"

#: ../src/yumex.py:639
msgid "No packages in transaction"
msgstr "トランザクション中のパッケージはありません"

#: ../src/yumex.py:649
msgid "Processing packages (See output for details)"
msgstr "パッケージを処理中 (詳細は出力を御覧下さい)"

#: ../src/yumex.py:667
msgid "Packages Processing"
msgstr "パッケージを処理中"

#: ../src/yumex.py:667
msgid "Packages Processing completed ok"
msgstr "パッケージ処理を完了"

#: ../src/yumex.py:671
msgid "Error in Transaction"
msgstr "トランザクション中にエラーが発生しました"

#: ../src/yumex.py:676
#, python-format
msgid "Parsing packages to %s "
msgstr "%sするパッケージを解析中"

#: ../src/yumex.py:710
#, python-format
msgid "found %d packages, matching : %s"
msgstr "%d個のパッケージを検出, 一致: %s"

#: ../src/yumex.py:777
msgid "Yum is locked by another application"
msgstr "Yumは別のアプリケーションによってロックされています"

#: ../src/yumex.py:779 ../src/yumex.py:806
msgid "Error in Yumex"
msgstr "Yumexのエラー"

#: ../src/yumex.py:787
msgid "Error in plugin, Yum Extender will exit"
msgstr "プラグインでエラーが発生しました。Yum Extenderは終了します。"

#: ../src/yumgui/callbacks.py:61
msgid "Erasing"
msgstr "消去中"

#: ../src/yumgui/callbacks.py:64 ../src/yumgui/callbacks.py:70
msgid "Obsoleted"
msgstr "廃止"

#: ../src/yumgui/callbacks.py:66
msgid "Updated"
msgstr "更新"

#: ../src/yumgui/callbacks.py:67
msgid "Erased"
msgstr "消去"

#: ../src/yumgui/callbacks.py:68 ../src/yumgui/callbacks.py:69
#: ../src/yumgui/callbacks.py:71 ../src/yumex.glade.h:36
msgid "Installed"
msgstr "インストール"

#: ../src/yumgui/callbacks.py:181
msgid "No header - huh?"
msgstr "ヘッダがありません、ん?"

#: ../src/yumex.glade.h:1
msgid " "
msgstr " "

#: ../src/yumex.glade.h:2
msgid "    "
msgstr "    "

#: ../src/yumex.glade.h:3
msgid "0"
msgstr "0"

#: ../src/yumex.glade.h:4
msgid "1"
msgstr "1"

#: ../src/yumex.glade.h:5
msgid "2"
msgstr "2"

#: ../src/yumex.glade.h:6
msgid "3"
msgstr "3"

#: ../src/yumex.glade.h:7
msgid "4"
msgstr "4"

#: ../src/yumex.glade.h:8
msgid "<b>Behavior</b>"
msgstr "<b>動作</b>"

#: ../src/yumex.glade.h:9
msgid "<b>Category</b>"
msgstr "<b>カテゴリ</b>"

#: ../src/yumex.glade.h:10
msgid "<b>Fonts </b>"
msgstr "<b>フォント</b>"

#: ../src/yumex.glade.h:11
msgid "<b>Packages</b>"
msgstr "<b>パッケージ</b>"

#: ../src/yumex.glade.h:12
msgid "<b>Quick add to queue</b>"
msgstr "<b>すぐにキューに追加</b>"

#: ../src/yumex.glade.h:13
msgid "<b>Repository  Exclude Filters</b>"
msgstr "<b>リポジトリ除外フィルタ</b>"

#: ../src/yumex.glade.h:14
msgid "<b>SUBACTION</b>\n"
msgstr "<b>SUBACTION</b>\n"

#: ../src/yumex.glade.h:16
msgid "<b>Total Download Size</b>"
msgstr "<b>合計ファイルサイズ</b>"

#: ../src/yumex.glade.h:17
msgid "<span size=\"large\"><b>Action</b></span>\n"
msgstr "<span size=\"large\" >動作</span>\n"

#: ../src/yumex.glade.h:19
msgid "<span size=\"xx-large\" color=\"black\">Group View</span>"
msgstr "<span size=\"xx-large\" color=\"black\">グループ</span>"

#: ../src/yumex.glade.h:20
msgid "<span size=\"xx-large\" color=\"black\">Output View</span>"
msgstr "<span size=\"xx-large\" color=\"black\">出力</span>"

#: ../src/yumex.glade.h:21
msgid "<span size=\"xx-large\" color=\"black\">Package Queue</span>"
msgstr "<span size=\"xx-large\" color=\"black\">パッケージキュー</span>"

#: ../src/yumex.glade.h:22
msgid "<span size=\"xx-large\" color=\"black\">Select Repositories</span>"
msgstr "<span size=\"xx-large\" color=\"black\">リポジトリの選択</span>"

#: ../src/yumex.glade.h:23
msgid "All"
msgstr "全て"

#: ../src/yumex.glade.h:25
msgid "Available"
msgstr "利用可能"

#: ../src/yumex.glade.h:26
msgid "C_hangelog"
msgstr "変更履歴(_H)"

#: ../src/yumex.glade.h:27
msgid "Console"
msgstr "コンソール"

#: ../src/yumex.glade.h:28
msgid "Debug _mode"
msgstr "デバッグモード(_M)"

#: ../src/yumex.glade.h:29
msgid "ETA"
msgstr "ETA"

#: ../src/yumex.glade.h:31
msgid "Excludes"
msgstr "除外"

#: ../src/yumex.glade.h:32
msgid "Extra"
msgstr "その他"

#: ../src/yumex.glade.h:33
msgid "Fi_les"
msgstr "ファイル(_L)"

#: ../src/yumex.glade.h:34
msgid "GUI"
msgstr "GUI"

#: ../src/yumex.glade.h:35
msgid "I_nformation"
msgstr "情報(_N)"

#: ../src/yumex.glade.h:37
msgid "Package Descriptions"
msgstr "パッケージの説明"

#: ../src/yumex.glade.h:38
msgid "Plugins"
msgstr "プラグイン"

#: ../src/yumex.glade.h:39
msgid "Preferences"
msgstr "設定"

#: ../src/yumex.glade.h:40
msgid "Process Queue Confirmation"
msgstr "処理キューの確認"

#: ../src/yumex.glade.h:41
msgid "Repos"
msgstr "Repos"

#: ../src/yumex.glade.h:42
msgid "Search Options"
msgstr "検索オプション"

#: ../src/yumex.glade.h:43
msgid "Updates"
msgstr "更新"

#: ../src/yumex.glade.h:44
msgid "Use _proxy"
msgstr "プロキシを使用する(_U)"

#: ../src/yumex.glade.h:45
msgid "Yum Extender"
msgstr "Yum Extender"

#: ../src/yumex.glade.h:46
msgid "Yum Extender Preferences"
msgstr "Yum Extenderの設定"

#: ../src/yumex.glade.h:47
msgid "_Add All"
msgstr "全て追加(_A)"

#: ../src/yumex.glade.h:48
msgid "_Auto refresh on start"
msgstr "全てをリフレッシュして開始(_A)"

#: ../src/yumex.glade.h:49
msgid "_Description"
msgstr "説明(_D)"

#: ../src/yumex.glade.h:50
msgid "_Deselect All"
msgstr "全て選択解除(_D)"

#: ../src/yumex.glade.h:51
msgid "_Edit"
msgstr "編集(_E)"

#: ../src/yumex.glade.h:52
msgid "_File"
msgstr "ファイル(_F)"

#: ../src/yumex.glade.h:53
msgid "_Help"
msgstr "ヘルプ(_H)"

#: ../src/yumex.glade.h:54
msgid "_Other"
msgstr "その他(_O)"

#: ../src/yumex.glade.h:55
msgid "_Process Queue"
msgstr "キューを処理する(_P)"

#: ../src/yumex.glade.h:56
msgid "_Profiles"
msgstr "プロファイル(_P)"

#: ../src/yumex.glade.h:57
msgid "_Remove All"
msgstr "全て削除(_R)"

#: ../src/yumex.glade.h:58
msgid "_Skip"
msgstr "スキップ(_S)"

#: ../src/yumex.glade.h:59
msgid "_Tools"
msgstr "ツール(_T)"

#: ../src/yumex.glade.h:60
msgid "_Yum Clean All"
msgstr "Yum全消去(_Y)"

#: ../src/yumex.glade.h:61
msgid "_Yum excludes:"
msgstr "Yum除外(_Y):"

#: ../src/yumex.glade.h:62
msgid "_Yum plugins:"
msgstr "Yumプラグイン(_Y):"

#: ../src/yumex.glade.h:63
msgid "dialog1"
msgstr "ダイアログ1"
