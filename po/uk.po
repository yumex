# Ukrainian translation of yumex.
# Copyright (C) 2005 Free Software Foundation
# This file is distributed under the same license as the yumex package.
# Maxim Dziumanenko <mvd@mylinux.ua>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: yumex 0.34\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-10-26 11:23+0200\n"
"PO-Revision-Date: 2006-03-21 12:06+0200\n"
"Last-Translator: Maxim Dziumanenko <mvd@mylinux.ua>\n"
"Language-Team: Ukrainian <uk@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/callbacks.py:69
#, python-format
msgid "Getting : %s"
msgstr "Отримання : %s"

#: ../src/callbacks.py:164 ../src/yumapi.py:204
msgid "Removing"
msgstr "Видалення"

#: ../src/callbacks.py:166
msgid "Cleanup"
msgstr "Очищення"

#: ../src/callbacks.py:184
#, fuzzy, python-format
msgid "Erased: %s"
msgstr "Видалено"

#: ../src/callbacks.py:205
#, python-format
msgid "Processing metadata from : %s"
msgstr "Обробка метаданих від : %s"

#: ../src/callbacks.py:207
msgid "Processing metadata"
msgstr "Обробка метаданих"

#: ../src/callbacks.py:234
#, fuzzy
msgid "installed"
msgstr "Встановлено"

#: ../src/callbacks.py:235
#, fuzzy
msgid "updated"
msgstr "Оновлено"

#: ../src/callbacks.py:236
#, fuzzy
msgid "obsoleted"
msgstr "Застарілі"

#: ../src/callbacks.py:237
#, fuzzy
msgid "erased"
msgstr "Видалено"

#: ../src/callbacks.py:240
#, python-format
msgid "---> Package %s.%s %s:%s-%s set to be %s"
msgstr "---> Пакет %s.%s %s:%s-%s буде %s"

#: ../src/callbacks.py:247
msgid "--> Running transaction check"
msgstr "--> Запускається перевірочна транзакція"

#: ../src/callbacks.py:251
msgid "--> Restarting Dependency Resolution with new changes."
msgstr "--> Перезапускається визначення залежностей з урахуванням змін."

#: ../src/callbacks.py:255
msgid "--> Finished Dependency Resolution"
msgstr "--> Визначення залежностей завершено"

#: ../src/callbacks.py:259
#, python-format
msgid "--> Processing Dependency: %s for package: %s"
msgstr "--> Обробка залежності: %s для пакету: %s"

#: ../src/callbacks.py:264
#, python-format
msgid "--> Unresolved Dependency: %s"
msgstr "--> Нерозв'язана залежність: %s"

#: ../src/callbacks.py:268
#, python-format
msgid "--> Processing Conflict: %s conflicts %s"
msgstr "--> Обробка конфлікту: %s конфліктів %s"

#: ../src/callbacks.py:271
msgid "--> Populating transaction set with selected packages. Please wait."
msgstr "--> Додавання вибраних пакетів до транзакції. Зачекайте, будь ласка."

#: ../src/callbacks.py:274
#, python-format
msgid "---> Downloading header for %s to pack into transaction set."
msgstr "---> У транзакцію завантажується заголовок пакету %s."

#: ../src/dialogs.py:196
#, fuzzy
msgid "Packages to Process"
msgstr "Обробка пакетів"

#: ../src/dialogs.py:212 ../src/views.py:474
msgid "Name"
msgstr "Назва"

#: ../src/dialogs.py:213 ../src/yumex.glade.h:24
msgid "Arch"
msgstr "Архітектура"

#: ../src/dialogs.py:214 ../src/views.py:61
msgid "Ver"
msgstr "Версія"

#. Setup reponame & repofile column's
#: ../src/dialogs.py:215 ../src/views.py:438 ../src/views.py:473
msgid "Repository"
msgstr "Репозиторій"

#: ../src/dialogs.py:216 ../src/views.py:61
msgid "Size"
msgstr "Розмір"

#: ../src/dialogs.py:348
#, fuzzy, python-format
msgid "About %s"
msgstr "Про програму"

#: ../src/gui.py:66
#, fuzzy, python-format
msgid ""
"Updating : %s\n"
"\n"
msgstr "Оновлення"

#: ../src/gui.py:73
#, fuzzy, python-format
msgid ""
"Obsoleting : %s\n"
"\n"
msgstr "Отримання : %s"

#: ../src/gui.py:88
msgid ""
"Requires:\n"
"\n"
msgstr ""
"Вимагається:\n"
"\n"

#: ../src/gui.py:103 ../src/gui.py:113
#, python-format
msgid "Can not read the %s attribute"
msgstr "Не вдається прочитати атрибут %s"

#: ../src/gui.py:369
#, fuzzy
msgid "Show All Packages"
msgstr "Пакети"

#: ../src/gui.py:370
#, fuzzy
msgid "Show Package Updates"
msgstr "%d пакетів для встановлення"

#: ../src/gui.py:371
#, fuzzy
msgid "Show available Packages"
msgstr "Пакети"

#: ../src/gui.py:372
#, fuzzy
msgid "Show Installed Packages"
msgstr "Завантажуються пакети:"

#. Setup Vertical Toolbar
#: ../src/gui.py:381
#, fuzzy
msgid "Package View"
msgstr "Пакет"

#: ../src/gui.py:382
msgid "Group View"
msgstr ""

#: ../src/gui.py:383
#, fuzzy
msgid "Package Queue View"
msgstr "Пакет"

#: ../src/gui.py:385
#, fuzzy
msgid "Repository Selection View"
msgstr "Помилка створення репозиторію"

#: ../src/gui.py:386
#, fuzzy
msgid "Output View"
msgstr "_Вивід"

#.
#. [1]           [2]               [3]             [4]     [5]     [6]
#. -------------------------------------------------------------------------
#: ../src/misc.py:93
msgid "RPM Groups"
msgstr "Групи RPM"

#: ../src/misc.py:94
msgid "Repositories"
msgstr "Репозиторії"

#: ../src/misc.py:95 ../src/views.py:61
msgid "Architecture"
msgstr "Архітектура"

#: ../src/misc.py:96
msgid "Sizes"
msgstr "Розмір"

#: ../src/misc.py:97
msgid "Age"
msgstr "Вік"

#: ../src/misc.py:107
#, fuzzy
msgid "Mandatory"
msgstr "Обов'_язкові"

#: ../src/misc.py:108
#, fuzzy
msgid "Default"
msgstr "_Типові"

#: ../src/misc.py:109
#, fuzzy
msgid "Optional"
msgstr "_Необов'язкові"

#: ../src/misc.py:110
#, fuzzy
msgid "Conditional"
msgstr "_Необов'язкові"

#: ../src/misc.py:118
msgid "Programming:"
msgstr ""

#: ../src/misc.py:121
#, fuzzy
msgid "Translation:"
msgstr "встановлення"

#: ../src/misc.py:142
msgid "Special Thanks To:"
msgstr ""

#: ../src/misc.py:359
#, fuzzy
msgid "Package Queue:"
msgstr "Пакет"

#: ../src/misc.py:364
#, fuzzy, python-format
msgid " Packages to %s"
msgstr "%d пакетів для встановлення"

#: ../src/misc.py:371
#, fuzzy, python-format
msgid " Groups to %s"
msgstr "Групи RPM"

#: ../src/misc.py:435
#, python-format
msgid "Wrong file application (%s)"
msgstr ""

#: ../src/misc.py:437
#, python-format
msgid "Wrong file type (%s)"
msgstr ""

#: ../src/misc.py:439
#, fuzzy, python-format
msgid "Wrong file version (%s)"
msgstr ""
"Встановлена версія: %s (%s) \n"
"\n"

#: ../src/misc.py:610
msgid "Current Settings :"
msgstr "Поточні параметри:"

#: ../src/views.py:40
#, fuzzy
msgid "Categories"
msgstr "_Категорія"

#: ../src/views.py:61 ../src/views.py:91
msgid "Package"
msgstr "Пакет"

#: ../src/views.py:61 ../src/views.py:94 ../src/views.py:218
msgid "Summary"
msgstr "Зведення"

#: ../src/views.py:61
msgid "Repo"
msgstr "Репозиторій"

#: ../src/views.py:92
msgid "Arch."
msgstr "Арх."

#: ../src/views.py:93
msgid "Ver."
msgstr "Версія"

#: ../src/views.py:95
msgid "Repo."
msgstr "Репо."

#: ../src/views.py:96
msgid "Size."
msgstr "Розм."

#: ../src/views.py:213
msgid "Packages"
msgstr "Пакети"

#: ../src/views.py:255
msgid "<b>Packages To Update</b>"
msgstr "<b>Пакети для оновлення</b>"

#: ../src/views.py:259
msgid "<b>Packages To Install</b>"
msgstr "<b>Пакети для встановлення</b>"

#: ../src/views.py:263
msgid "<b>Packages To Remove</b>"
msgstr "<b>Пакети для видалення</b>"

#: ../src/views.py:438
#, fuzzy
msgid "Filename"
msgstr "Файл"

#: ../src/views.py:571
msgid "Plugin"
msgstr "Модуль"

#: ../src/yumapi.py:67 ../src/yumapi.py:114
msgid "Setup Yum : Transaction Set"
msgstr "Налаштовування Yum: транзакція"

#: ../src/yumapi.py:69 ../src/yumapi.py:116
msgid "Setup Yum : RPM Db."
msgstr "Налаштовування Yum : база даних RPM"

#. -> Repo Setup
#: ../src/yumapi.py:72 ../src/yumapi.py:119
#, fuzzy
msgid "Setup Yum : Repositories."
msgstr "Налаштовування Yum : конфігурація"

#. -> Sack Setup
#: ../src/yumapi.py:75 ../src/yumapi.py:122
#, fuzzy
msgid "Setup Yum : Package Sacks"
msgstr "Встановлення стеку пакетів"

#. -> Updates Setup
#. -> Sack Setup
#: ../src/yumapi.py:78 ../src/yumapi.py:125
#, fuzzy
msgid "Setup Yum : Updates"
msgstr "Налаштовування Yum : модулі"

#. -> Group Setup
#. -> Sack Setup
#: ../src/yumapi.py:82 ../src/yumapi.py:128
#, fuzzy
msgid "Setup Yum : Groups"
msgstr "Налаштовування Yum : модулі"

#: ../src/yumapi.py:84 ../src/yumapi.py:130
msgid "Setup Yum : Base setup completed"
msgstr "Налаштовування Yum : базове налаштовування завершено"

#: ../src/yumapi.py:138
#, fuzzy, python-format
msgid "Loaded update Metadata from %s "
msgstr "Завантажувати метадані yum"

#: ../src/yumapi.py:140
#, python-format
msgid "No update Metadata found for %s "
msgstr ""

#: ../src/yumapi.py:159 ../src/yumex.py:601
msgid "Preparing for install/remove/update"
msgstr "Підготовка до встановлення/видалення/оновлення"

#: ../src/yumapi.py:163
msgid "--> Preparing for install"
msgstr "--> Підготовка до встановлення"

#: ../src/yumapi.py:168
msgid "--> Preparing for remove"
msgstr "--> Підготовка до видалення"

#: ../src/yumapi.py:175
msgid "--> Preparing for a full update"
msgstr "--> Підготовка до повного оновлення"

#: ../src/yumapi.py:178
msgid "--> Preparing for a partial update"
msgstr "--> Підготовка до часткового оновлення"

#: ../src/yumapi.py:180
msgid "--> Adding all obsoletion to transaction"
msgstr "--> Додавання у транзакцію списку застарілих пакетів"

#: ../src/yumapi.py:202 ../src/yumgui/callbacks.py:62
#: ../src/yumgui/callbacks.py:63 ../src/yumgui/callbacks.py:65
msgid "Installing"
msgstr "Встановлення"

#: ../src/yumapi.py:203 ../src/yumgui/callbacks.py:60
msgid "Updating"
msgstr "Оновлення"

#: ../src/yumapi.py:205
msgid "Installing for dependencies"
msgstr "Встановлення для залежностей"

#: ../src/yumapi.py:206
msgid "Updating for dependencies"
msgstr "Оновлення для залежностей"

#: ../src/yumapi.py:207
msgid "Removing for dependencies"
msgstr "Видалення для залежностей"

#: ../src/yumapi.py:253
#, python-format
msgid "Failure getting %s: "
msgstr "Помилка отримання %s:"

#: ../src/yumapi.py:275
msgid "Downloading Packages:"
msgstr "Завантажуються пакети:"

#: ../src/yumapi.py:291 ../src/yumapi.py:299
msgid "Error Downloading Packages:\n"
msgstr "Помилка завантаження пакетів:\n"

#. Check GPG signatures
#: ../src/yumapi.py:306
msgid "Checking GPG Signatures:"
msgstr "Перевірка підписів GPG:"

#: ../src/yumapi.py:311
msgid "Error checking package signatures:\n"
msgstr "Помилка перевірки сигнатур пакетів:\n"

#: ../src/yumapi.py:317
#, fuzzy
msgid "Traceback in checking package signatures:\n"
msgstr "Помилка перевірки сигнатур пакетів:\n"

#: ../src/yumapi.py:324
msgid "Running Transaction Test"
msgstr "Запускається перевірочна транзакція"

#: ../src/yumapi.py:342
msgid "Finished Transaction Test"
msgstr "Перевірочна транзакція завершена"

#: ../src/yumapi.py:344
msgid "Transaction Check Error: "
msgstr "Помилка у перевірочній транзакції"

#: ../src/yumapi.py:349
msgid "Transaction Test Succeeded"
msgstr "Перевірочна транзакція успішно завершена"

#: ../src/yumapi.py:365
msgid "Running Transaction"
msgstr "Запуск транзакції"

#. print "po: %s userid: %s hexkey: %s " % (str(po),userid,hexkeyid)
#: ../src/yumapi.py:449
#, python-format
msgid "Do you want to import GPG Key : %s \n"
msgstr ""

#: ../src/yumapi.py:451
#, python-format
msgid "Needed by %s"
msgstr ""

#: ../src/yumex.py:63
#, fuzzy
msgid "Cant Quit while running RPM Transactions"
msgstr "Запуск транзакції"

#: ../src/yumex.py:68
msgid "Quiting, please wait !!!!!!"
msgstr ""

#: ../src/yumex.py:158
msgid "Yum has not been initialized yet"
msgstr ""

#. Check there are any packages in the queue
#: ../src/yumex.py:164
#, fuzzy
msgid "No packages in queue"
msgstr "Обробка пакетів у черзі"

#: ../src/yumex.py:241
#, python-format
msgid "You are about to add %s packages\n"
msgstr ""

#: ../src/yumex.py:242 ../src/yumex.py:254
msgid "It will take some time\n"
msgstr ""

#: ../src/yumex.py:243 ../src/yumex.py:255
#, fuzzy
msgid "do you want to continue ?"
msgstr "Бажаєте зберегти зміни ?"

#: ../src/yumex.py:253
#, python-format
msgid "You are about to remove %s packages\n"
msgstr ""

#: ../src/yumex.py:359
#, python-format
msgid "Selected the %s profile"
msgstr "Вибрано профіль %s"

#: ../src/yumex.py:379
#, python-format
msgid "Profile : %s saved ok"
msgstr "Профіль : %s успішно збережено"

#: ../src/yumex.py:381
#, python-format
msgid "Profile : %s save failed"
msgstr "Профіль : помилка збереження %s"

#: ../src/yumex.py:386
msgid "Create New Profile"
msgstr "Створення нового профілю"

#: ../src/yumex.py:386
#, fuzzy
msgid "Name of new profile"
msgstr "Створення нового профілю"

#: ../src/yumex.py:390
#, fuzzy, python-format
msgid "Profile : %s created ok"
msgstr "Профіль : %s створено"

#: ../src/yumex.py:393
#, python-format
msgid "Profile : %s creation failed"
msgstr "Профіль : помилка створення %s"

#: ../src/yumex.py:397
msgid "Cleaning up all yum metadata"
msgstr ""

#: ../src/yumex.py:424
msgid "Yum Config Setup"
msgstr ""

#: ../src/yumex.py:440
msgid "GUI Setup Completed"
msgstr ""

#: ../src/yumex.py:482 ../src/yumex.py:616 ../src/yumex.py:671
#: ../src/yumex.py:779 ../src/yumex.py:787 ../src/yumex.py:806
#: ../src/yumex.glade.h:30
msgid "Error"
msgstr "Помилка"

#: ../src/yumex.py:482
#, fuzzy
msgid "Error in Yum Setup"
msgstr "Помилка у параметрах yum"

#. prepare package lists
#. -> List setup
#: ../src/yumex.py:487
#, fuzzy
msgid "Building Package Lists"
msgstr "Створення списку пакетів завершено"

#: ../src/yumex.py:489
#, fuzzy
msgid "Building Package Lists Completed"
msgstr "Створення списку пакетів завершено"

#: ../src/yumex.py:490
msgid "Building Groups Lists"
msgstr ""

#: ../src/yumex.py:493
#, fuzzy
msgid "Building Group Lists Completed"
msgstr "Створення списку пакетів завершено"

#: ../src/yumex.py:525
#, fuzzy, python-format
msgid "Getting packages : %s"
msgstr "Встановлення стеку пакетів"

#: ../src/yumex.py:529
#, python-format
msgid "Found %d %s packages"
msgstr ""

#. -> Sort Lists
#: ../src/yumex.py:532
#, fuzzy
msgid "Sorting packages"
msgstr "Встановлення стеку пакетів"

#: ../src/yumex.py:534
#, fuzzy
msgid "Population view with packages"
msgstr "Заповнення пакетами %s : (%-4s / %-4s)"

#: ../src/yumex.py:540
#, fuzzy
msgid "Population Completed"
msgstr "Заповнення списку завершено"

#: ../src/yumex.py:547
#, fuzzy
msgid "Package View Population"
msgstr "Опис пакету"

#: ../src/yumex.py:559
#, fuzzy
msgid "Package View Population Completed"
msgstr "Заповнення списку завершено"

#: ../src/yumex.py:565
msgid "Category View Population"
msgstr ""

#: ../src/yumex.py:584
#, fuzzy
msgid "Category View Population Completed"
msgstr "Заповнення списку завершено"

#: ../src/yumex.py:609
msgid "Processing Packages in queue"
msgstr "Обробка пакетів у черзі"

#: ../src/yumex.py:615
msgid "Error in Dependency Resolution"
msgstr "Помилка розв'язання залежностей"

#: ../src/yumex.py:632
msgid "No packages selected"
msgstr "Пакети не вибрані"

#: ../src/yumex.py:639
#, fuzzy
msgid "No packages in transaction"
msgstr "Обробка пакетів у черзі"

#: ../src/yumex.py:649
msgid "Processing packages (See output for details)"
msgstr "Обробка пакетів (дивіться вивід)"

#: ../src/yumex.py:667
msgid "Packages Processing"
msgstr "Обробка пакетів"

#: ../src/yumex.py:667
msgid "Packages Processing completed ok"
msgstr "Обробку пакетів пакетів успішно завершено"

#: ../src/yumex.py:671
msgid "Error in Transaction"
msgstr "Помилка транзакції"

#: ../src/yumex.py:676
#, fuzzy, python-format
msgid "Parsing packages to %s "
msgstr "%d пакетів для встановлення"

#: ../src/yumex.py:710
#, python-format
msgid "found %d packages, matching : %s"
msgstr ""

#: ../src/yumex.py:777
msgid "Yum is locked by another application"
msgstr "Yum заблокований іншою програмою"

#: ../src/yumex.py:779 ../src/yumex.py:806
#, fuzzy
msgid "Error in Yumex"
msgstr "Помилка у параметрах yum"

#: ../src/yumex.py:787
msgid "Error in plugin, Yum Extender will exit"
msgstr ""

#: ../src/yumgui/callbacks.py:61
msgid "Erasing"
msgstr "Видалення"

#: ../src/yumgui/callbacks.py:64 ../src/yumgui/callbacks.py:70
msgid "Obsoleted"
msgstr "Застарілі"

#: ../src/yumgui/callbacks.py:66
msgid "Updated"
msgstr "Оновлено"

#: ../src/yumgui/callbacks.py:67
msgid "Erased"
msgstr "Видалено"

#: ../src/yumgui/callbacks.py:68 ../src/yumgui/callbacks.py:69
#: ../src/yumgui/callbacks.py:71 ../src/yumex.glade.h:36
msgid "Installed"
msgstr "Встановлено"

#: ../src/yumgui/callbacks.py:181
msgid "No header - huh?"
msgstr "Відсутній заголовок - як це?"

#: ../src/yumex.glade.h:1
msgid " "
msgstr " "

#: ../src/yumex.glade.h:2
msgid "    "
msgstr "    "

#: ../src/yumex.glade.h:3
msgid "0"
msgstr "0"

#: ../src/yumex.glade.h:4
msgid "1"
msgstr "1"

#: ../src/yumex.glade.h:5
msgid "2"
msgstr "2"

#: ../src/yumex.glade.h:6
msgid "3"
msgstr "3"

#: ../src/yumex.glade.h:7
msgid "4"
msgstr "4"

#: ../src/yumex.glade.h:8
msgid "<b>Behavior</b>"
msgstr "<b>Поведінка</b>"

#: ../src/yumex.glade.h:9
#, fuzzy
msgid "<b>Category</b>"
msgstr "<b>Ознаки</b>"

#: ../src/yumex.glade.h:10
msgid "<b>Fonts </b>"
msgstr "<b>Шрифти</b>"

#: ../src/yumex.glade.h:11
#, fuzzy
msgid "<b>Packages</b>"
msgstr "<b>Пакети для оновлення</b>"

#: ../src/yumex.glade.h:12
#, fuzzy
msgid "<b>Quick add to queue</b>"
msgstr "до черги додано %i пакетів"

#: ../src/yumex.glade.h:13
msgid "<b>Repository  Exclude Filters</b>"
msgstr ""

#: ../src/yumex.glade.h:14
msgid "<b>SUBACTION</b>\n"
msgstr "<b>ПІДДІЯ</b>\n"

#: ../src/yumex.glade.h:16
msgid "<b>Total Download Size</b>"
msgstr "<b>Загальний обсяг завантаження</b>"

#: ../src/yumex.glade.h:17
msgid "<span size=\"large\"><b>Action</b></span>\n"
msgstr "<span size=\"large\"><b>Дія</b></span>\n"

#: ../src/yumex.glade.h:19
msgid "<span size=\"xx-large\" color=\"black\">Group View</span>"
msgstr "<span size=\"xx-large\" color=\"black\">Перегляд груп</span>"

#: ../src/yumex.glade.h:20
msgid "<span size=\"xx-large\" color=\"black\">Output View</span>"
msgstr "<span size=\"xx-large\" color=\"black\">Перегляд виводу</span>"

#: ../src/yumex.glade.h:21
msgid "<span size=\"xx-large\" color=\"black\">Package Queue</span>"
msgstr "<span size=\"xx-large\" color=\"black\">Черга пакетів</span>"

#: ../src/yumex.glade.h:22
msgid "<span size=\"xx-large\" color=\"black\">Select Repositories</span>"
msgstr "<span size=\"xx-large\" color=\"black\">Вибір репозиторіїв</span>"

#: ../src/yumex.glade.h:23
msgid "All"
msgstr ""

#: ../src/yumex.glade.h:25
msgid "Available"
msgstr ""

#: ../src/yumex.glade.h:26
msgid "C_hangelog"
msgstr "Історія _змін"

#: ../src/yumex.glade.h:27
msgid "Console"
msgstr "Консоль"

#: ../src/yumex.glade.h:28
msgid "Debug _mode"
msgstr "Режим _налагодження"

#: ../src/yumex.glade.h:29
msgid "ETA"
msgstr "Очікуваний час"

#: ../src/yumex.glade.h:31
msgid "Excludes"
msgstr "Винятки"

#: ../src/yumex.glade.h:32
msgid "Extra"
msgstr "Додатково"

#: ../src/yumex.glade.h:33
msgid "Fi_les"
msgstr "Фа_йли"

#: ../src/yumex.glade.h:34
msgid "GUI"
msgstr "Графічний інтерфейс"

#: ../src/yumex.glade.h:35
msgid "I_nformation"
msgstr "Інфо_рмація"

#: ../src/yumex.glade.h:37
msgid "Package Descriptions"
msgstr "Опис пакету"

#: ../src/yumex.glade.h:38
msgid "Plugins"
msgstr "Модулі"

#: ../src/yumex.glade.h:39
msgid "Preferences"
msgstr "Параметри"

#: ../src/yumex.glade.h:40
msgid "Process Queue Confirmation"
msgstr "Підтвердження обробки черги"

#: ../src/yumex.glade.h:41
#, fuzzy
msgid "Repos"
msgstr "Р_епозиторії"

#: ../src/yumex.glade.h:42
msgid "Search Options"
msgstr ""

#: ../src/yumex.glade.h:43
#, fuzzy
msgid "Updates"
msgstr "Оновити"

#: ../src/yumex.glade.h:44
msgid "Use _proxy"
msgstr "Використовувати п_роксі"

#: ../src/yumex.glade.h:45
#, fuzzy
msgid "Yum Extender"
msgstr "Про програму: Yum Extender"

#: ../src/yumex.glade.h:46
msgid "Yum Extender Preferences"
msgstr "Параметри Yum Extender"

#: ../src/yumex.glade.h:47
msgid "_Add All"
msgstr ""

#: ../src/yumex.glade.h:48
msgid "_Auto refresh on start"
msgstr "_Автоматично оновлювати на старті"

#: ../src/yumex.glade.h:49
msgid "_Description"
msgstr "Оп_ис"

#: ../src/yumex.glade.h:50
msgid "_Deselect All"
msgstr "Зн_яти виділення"

#: ../src/yumex.glade.h:51
msgid "_Edit"
msgstr "_Правка"

#: ../src/yumex.glade.h:52
msgid "_File"
msgstr "_Файл"

#: ../src/yumex.glade.h:53
msgid "_Help"
msgstr "_Довідка"

#: ../src/yumex.glade.h:54
msgid "_Other"
msgstr "Ін_ші"

#: ../src/yumex.glade.h:55
msgid "_Process Queue"
msgstr "_Обробити чергу"

#: ../src/yumex.glade.h:56
msgid "_Profiles"
msgstr "Профі_лі"

#: ../src/yumex.glade.h:57
#, fuzzy
msgid "_Remove All"
msgstr "В_идалити"

#: ../src/yumex.glade.h:58
msgid "_Skip"
msgstr ""

#: ../src/yumex.glade.h:59
msgid "_Tools"
msgstr "Панел_ь інструментів"

#: ../src/yumex.glade.h:60
msgid "_Yum Clean All"
msgstr ""

#: ../src/yumex.glade.h:61
msgid "_Yum excludes:"
msgstr "_Винятки Yum:"

#: ../src/yumex.glade.h:62
msgid "_Yum plugins:"
msgstr "_Модулі Yum:"

#: ../src/yumex.glade.h:63
msgid "dialog1"
msgstr "dialog1"

#~ msgid "About Yum Extender"
#~ msgstr "Про програму: Yum Extender"

#, fuzzy
#~ msgid "Groups"
#~ msgstr "_Групи"

#~ msgid "<b>(C) 2005  - Tim Lauridsen</b>"
#~ msgstr "<b>(C) 2005  - Tim Lauridsen</b>"

#~ msgid "<b>Flags</b>"
#~ msgstr "<b>Ознаки</b>"

#~ msgid "<b>GPG Key</b>"
#~ msgstr "<b>Ключ GPG</b>"

#~ msgid "<b>Select Profile</b>"
#~ msgstr "<b>Виберіть профіль</b>"

#~ msgid "<b>URL Type</b>"
#~ msgstr "<b>Тип URL</b>"

#~ msgid "<b>URLs</b>"
#~ msgstr "<b>Посилання URL</b>"

#~ msgid "Credits"
#~ msgstr "Подяки"

#~ msgid "Debug mode"
#~ msgstr "Режим налагодження"

#~ msgid "Disable 'Yum Extender Launcher'"
#~ msgstr "Вимкнути 'Запуск Yum Extender'"

#~ msgid "Disable Plugins"
#~ msgstr "Вимкнути модулі"

#~ msgid "Edit Repository"
#~ msgstr "Правка репозиторію"

#~ msgid "Update Only"
#~ msgstr "Лише оновлення"

#~ msgid "Yum Extender Launcher"
#~ msgstr "Запуск Yum Extender"

#~ msgid "_Base url "
#~ msgstr "_Базовий url"

#~ msgid "_Enabled"
#~ msgstr "_Увімкнено"

#~ msgid "_GPG check"
#~ msgstr "Перевіряти _GPG"

#~ msgid "_Mirror list"
#~ msgstr "Перелік д_зеркал"

#~ msgid "_Name:"
#~ msgstr "_Назва:"

#~ msgid "_Tag:"
#~ msgstr "_Тег:"

#~ msgid "xxxx"
#~ msgstr "xxxx"

#, fuzzy
#~ msgid "Save"
#~ msgstr "З_берегти"

#~ msgid "_About"
#~ msgstr "_Про програму"

#~ msgid "_Delete from Queue"
#~ msgstr "В_идалити з черги"

#~ msgid "_New"
#~ msgstr "_Створити"

#~ msgid "_Quit"
#~ msgstr "Ви_йти"

#~ msgid "_Save"
#~ msgstr "З_берегти"

#, fuzzy
#~ msgid ""
#~ "Installed version: %s\n"
#~ "\n"
#~ msgstr ""
#~ "Встановлена версія: %s (%s) \n"
#~ "\n"

#, fuzzy
#~ msgid "Quiting"
#~ msgstr "оновлення"

#~ msgid "Process (_SafeMode)"
#~ msgstr "Обробка (_безпечний режим)"

#~ msgid "_Reset Repository Cache"
#~ msgstr "О_чистити кеш репозиторію"

#~ msgid "(yumex) Erased: %s"
#~ msgstr "(yumex) Видалено: %s"

#~ msgid "<b>Mirror Detection</b>"
#~ msgstr "<b>Виявлення дзеркал</b>"

#~ msgid "Install Repository"
#~ msgstr "Встановити репозиторій"

#~ msgid "_Best"
#~ msgstr "Най_краще"

#~ msgid "_Default"
#~ msgstr "_Типові"

#~ msgid "_Enable 'Process Queue (SafeMode)'"
#~ msgstr "_Увімкнути обробку черги (безпечний режим)"

#~ msgid "_Fastest"
#~ msgstr "Най_швидше"

#~ msgid "_Show file lists & changelog (longer metadata load time)"
#~ msgstr ""
#~ "_Показувати списки файлів та списки змін (більший час завантаження "
#~ "метаданих)"

#, fuzzy
#~ msgid "_Packages"
#~ msgstr "Пакети"

#~ msgid "_Queue"
#~ msgstr "_Черга"

#~ msgid "Filte_r:"
#~ msgstr "Ф_ільтр:"

#~ msgid "Sear_ch"
#~ msgstr "З_найти"

#~ msgid "_Select All"
#~ msgstr "Виді_лити все"

#~ msgid "_Delete"
#~ msgstr "В_идалити"

#~ msgid " Do you want to delete the <b>%s</b> repository?"
#~ msgstr "Бажаєте видалити репозиторій <b>%s</b>?"

#~ msgid "Delete Repository"
#~ msgstr "Видалення репозиторію"

#~ msgid "_Add to Queue"
#~ msgstr "_Додати до черги"

#~ msgid "added remove of %s to Queue"
#~ msgstr "додано операцію видалення %s"

#~ msgid "added install of %s to Queue"
#~ msgstr "додано операцію встановлення %s"

#~ msgid "%i packages added to queue for removal"
#~ msgstr "у черзі %i пакетів для видалення"

#~ msgid "package %s was not found"
#~ msgstr "пакет %s не існує"

#~ msgid "%i packages added to queue for installation"
#~ msgstr "у черзі %i пакетів для видалення"

#~ msgid "Profile %s not found"
#~ msgstr "Профіль %s не існує"

#~ msgid "No profile selected"
#~ msgstr "Профіль не вибрано"

#~ msgid "_Profile name:"
#~ msgstr "Назва _профілю:"

#~ msgid "_Update"
#~ msgstr "_Оновити"

#~ msgid "_Install"
#~ msgstr "_Встановити"

#~ msgid "Updating %s view, populate view with data "
#~ msgstr "Оновлення %s, заповнення даними"

#~ msgid "Using http_proxy=%s"
#~ msgstr "Використовується http_proxy=%s"

#~ msgid "Initializing Repositories"
#~ msgstr "Ініціалізація репозиторіїв"

#~ msgid "Repository initialization completed in %.2f seconds"
#~ msgstr "Ініціалізація Yum завершиться через %.2f секунд"

#~ msgid "Error in loading repository data"
#~ msgstr "Помилка завантаження даних репозиторію"

#~ msgid "Mirrordetection : %s"
#~ msgstr "Вибір дзеркала : %s"

#~ msgid "Yum Version : %s (%s)"
#~ msgstr "Версія yum: %s (%s)"

#~ msgid "Setup Yum : Plugins are disabled"
#~ msgstr "Налаштовування Yum : модулі вимкнено"

#~ msgid "Using yum cache"
#~ msgstr "Використовується кеш yum"

#~ msgid "Loading Repositories Data : Init. %s repository"
#~ msgstr "Завантаження даних репозиторію : ініціалізація %s"

#~ msgid "Reading mirror URL's from yumex cache"
#~ msgstr "Читаються URL дзеркал з кешу yumex"

#~ msgid "Determining %s mirrors for %s"
#~ msgstr "Визначення %s дзеркал для %s"

#~ msgid "--> Mirrors Detected (Top 3)"
#~ msgstr "--> Визначено дзеркал (Перші 3)"

#~ msgid "Using default mirror detection"
#~ msgstr "Використовується визначення типового дзеркала"

#~ msgid "Loading Repositories Data"
#~ msgstr "Завантаження даних репозиторію"

#~ msgid "Setting Up Package Sack : Filelists"
#~ msgstr "Встановлення стеку пакетів : список файлів"

#~ msgid "Setting Up Package Sack : Changelogs"
#~ msgstr "Встановлення стеку пакетів : історія змін"

#~ msgid "Loading Repository Group Data"
#~ msgstr "Завантаження даних про групи репозиторію"

#~ msgid "Loading Repositories Data completed"
#~ msgstr "Завантаження даних репозиторію завершено"

#~ msgid "Writing Mirrorcache to disk"
#~ msgstr "Запис кеша дзеркал на диск"

#~ msgid "--> Repositories saved: %s"
#~ msgstr "--> Збережено репозиторіїв: %s"

#~ msgid "Enable/Disable Repositories"
#~ msgstr "Вмикання/вимикання репозиторіїв"

#~ msgid "Building Package Lists : Updates"
#~ msgstr "Створення списку пакетів : оновлення"

#~ msgid "Building Package Lists : %i Updates found"
#~ msgstr "Створення списку пакетів : є %i оновлень"

#~ msgid "Building Package Lists : Installed"
#~ msgstr "Створення списку пакетів : встановлені"

#~ msgid "Building Package Lists : %i Installed packages found"
#~ msgstr "Створення списку пакетів : встановлено %i пакетів"

#~ msgid "Building Package Lists : Available"
#~ msgstr "Створення списку пакетів : доступні"

#~ msgid "Building Package Lists : %i Available packages found"
#~ msgstr "Створення списку пакетів : доступно %i пакетів"

#~ msgid "Retrieving GPG key from %s"
#~ msgstr "Отримується GPG ключ для %s"

#~ msgid "GPG key retrieval failed: "
#~ msgstr "Помилка отримання ключа GPG: "

#~ msgid "GPG key parsing failed: "
#~ msgstr "Помилка розбору ключа GPG: "

#~ msgid "GPG key at %s (0x%s) is already installed"
#~ msgstr "Ключ GPG з %s (0x%s) вже встановлено"

#~ msgid "Importing GPG key 0x%s \"%s\""
#~ msgstr "Імпортується ключ GPG 0x%s \"%s\""

#~ msgid "Key import failed (code %d)"
#~ msgstr "Помилка імпорту ключа (код %d)"

#~ msgid "Key imported successfully"
#~ msgstr "Ключ успішно імпортовано"

#~ msgid ""
#~ "The GPG keys listed for the \"%s\" repository are already installed but "
#~ "they are not correct for this package.\n"
#~ "Check that the correct key URLs are configured for this repository."
#~ msgstr ""
#~ "Перелічені у репозиторії \"%s\" ключі GPG вже встановлені, але вони не є "
#~ "коректними для цього пакету.\n"
#~ "Перевірте, що для цього репозиторію налаштовано коректні URL для ключів."

#~ msgid "Import of key(s) didn't help, wrong key(s)?"
#~ msgstr "Імпортування ключів не допомагає, неправильний ключ?"

#~ msgid "Warning: Group %s does not exist."
#~ msgstr "Попередження: група %s не існує."

#~ msgid "No packages in any requested group available to install or update"
#~ msgstr "Для встановлення або оновлення немає пакетів у жодній групі"

#~ msgid "No group named %s exists"
#~ msgstr "Група з назвою %s не існує"

#~ msgid "No packages to remove from groups"
#~ msgstr "Відсутні пакети для видалення з груп"

#~ msgid "%d Package(s) to remove"
#~ msgstr "%d пакетів для видалення"

#~ msgid "%s was not deleted"
#~ msgstr "%s не було видалено"

#~ msgid "Preparing for install Groups"
#~ msgstr "Підготовка до встановлення груп"

#~ msgid "Installing Groups"
#~ msgstr "Встановлення груп"

#~ msgid "Error in preparation for package installing"
#~ msgstr "Помилка підготовки до встановлення пакетів"

#~ msgid "Install"
#~ msgstr "Встановити"

#~ msgid "Preparing for Removing Groups"
#~ msgstr "Підготовка до видалення груп"

#~ msgid "Removeing Groups"
#~ msgstr "Видалення груп"

#~ msgid "Error in preparation for package removing"
#~ msgstr "Помилка підготовки до видалення пакетів"

#~ msgid "Remove"
#~ msgstr "Видалити"

#~ msgid ""
#~ "Enabled Repositories:\n"
#~ "%s"
#~ msgstr ""
#~ "Увімкнено репозиторії:\n"
#~ "%s"

#~ msgid "default"
#~ msgstr "типово"

#~ msgid "fastest"
#~ msgstr "найшвидше"

#~ msgid "best"
#~ msgstr "найкраще"

#~ msgid "removal"
#~ msgstr "видалення"

#~ msgid "<span size=\"xx-large\" color=\"black\">Install Packages</span>"
#~ msgstr "<span size=\"xx-large\" color=\"black\">Встановлення пакетів</span>"

#~ msgid "<span size=\"xx-large\" color=\"black\">Remove Packages</span>"
#~ msgstr "<span size=\"xx-large\" color=\"black\">Видалення пакетів</span>"

#~ msgid "<span size=\"xx-large\" color=\"black\">Packages by category</span>"
#~ msgstr "<span size=\"xx-large\" color=\"black\">Пакети за категорією</span>"

#~ msgid "<span size=\"xx-large\" color=\"black\">Update Packages</span>"
#~ msgstr "<span size=\"xx-large\" color=\"black\">Оновлення пакетів</span>"

#~ msgid ""
#~ "\n"
#~ "You are using the <b>--forcei386</b> option which makes yumex\n"
#~ "to use i386 repositories instead of x86_64. Use it with\n"
#~ "<b>extreme caution</b> because packages installed in this\n"
#~ "mode <b>will not be upgraded</b> during normal operation\n"
#~ "of yumex, yum or other yum-based tools. This may lead to\n"
#~ "<b>package conflicts</b> which <b>could</b> only be resolved\n"
#~ "by running yumex with this option again and updating i386\n"
#~ "packages to their newest versions.\n"
#~ "<b>***DO NOT USE THIS OPTION*** IF YOU WANT AVOID PROBLEMS IN THE FUTURE</"
#~ "b>\n"
#~ msgstr ""
#~ "\n"
#~ "Ви вказали параметр <b>--forcei386</b>, що змушує yumex\n"
#~ "використовувати репозиторії i386 замість x86_64. Користуйтесь цим\n"
#~ "<b>дуже обережно</b>, оскільки встановлені у цьому режимі пакети\n"
#~ "<b>не будуть оновлюватись</b> при звичайному використанні\n"
#~ "yumex, yum або інших засобах, побудованих на yum. Це може призвести\n"
#~ "до <b>конфлікту пакетів</b>, який <b>можна</b> вирішити\n"
#~ "лише знову запустивши yumex з цим параметром для оновлення пакетів\n"
#~ "i386 на їх більш нові версії.\n"
#~ "<b>***НЕ КОРИСТУЙТЕСЬ ЦИМ ПАРАМЕТРОМ*** ЩОБ УНИКНУТИ ПРОБЛЕМ У "
#~ "МАЙБУТНЬОМУ</b>\n"

#~ msgid "New Repository"
#~ msgstr "Новий репозиторій"

#~ msgid "_New repository tag name:"
#~ msgstr "Назва тегу _нового репозиторію:"

#~ msgid "Save Changes"
#~ msgstr "Зберегти зміни"

#~ msgid "<span size='x-large'> Packages to Process</span>"
#~ msgstr "<span size='x-large'> Пакети для обробки </span>"

#~ msgid "%s view is populated"
#~ msgstr "список %s заповнено"

#~ msgid "Categories / Groups"
#~ msgstr "Категорії / Групи"

#~ msgid "Description"
#~ msgstr "Опис"

#~ msgid "All Groups"
#~ msgstr "Усі групи"

#~ msgid "A_dd to Queue"
#~ msgstr "Додати до _черги"

#~ msgid "Test"
#~ msgstr "Перевірка"

#~ msgid "_Add & Process"
#~ msgstr "Додати та о_бробити"
